package Libs.Utils;

public class utils {
    public static final Boolean DEBUG = false;
    /**
     * *******************************************************
     * Method: sysPrint() is a debugging aid that is used to print
     * information to the screen.
     ********************************************************/
    public static void debugMessage(String str) {
        if (utils.DEBUG)
            System.out.println(str);
    }
    public static void debugMessage(String ...str) {
        if (utils.DEBUG)
            for(String message: str)
                System.out.println(message+"\n");

    }
}
