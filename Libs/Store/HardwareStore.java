/*
 * **************************************************
 * GROUPE :
 * - Marc Terence NDONG NZE
 * - Julien LAURET
 * - M�linda CLERVILLE
 * <p>
 * File:  HardwareStore.java
 * <p>
 * This program reads a random access file sequentially,
 * diagUpdates data already written to the file, creates new
 * data to be placed in the file, and deletes data
 * already in the file.
 * <p>
 * <p>
 * Copyright (c) 2002-2003 Advanced Applications Total Applications Works.
 * (AATAW)  All Rights Reserved.
 * <p>
 * AATAW grants you ("Licensee") a non-exclusive, royalty free, license to use,
 * modify and redistribute this software in source and binary code form,
 * provided that i) this copyright notice and license appear on all copies of
 * the software; and ii) Licensee does not utilize the software in a manner
 * which is disparaging to AATAW.
 * <p>
 * This software is provided "AS IS," without a warranty of any kind. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. AATAW AND ITS LICENSORS SHALL NOT BE
 * LIABLE FOR ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF USING, MODIFYING
 * OR DISTRIBUTING THE SOFTWARE OR ITS DERIVATIVES. IN NO EVENT WILL AATAW OR ITS
 * LICENSORS BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR DIRECT,
 * INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES, HOWEVER
 * CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING OUT OF THE USE OF
 * OR INABILITY TO USE SOFTWARE, EVEN IF SUN HAS BEEN ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGES.
 * <p>
 * This software is not designed or intended for use in on-line control of
 * aircraft, air traffic, aircraft navigation or aircraft communications; or in
 * the design, construction, operation or maintenance of any nuclear
 * facility. Licensee represents and warrants that it will not use or
 * redistribute the Software for such purposes.
 */

package Libs.Store;

import Libs.Handler.MenuHandler;
import Libs.Handler.MouseClickedHandler;
import Libs.Handler.WindowHandler;
import Libs.Record.DeleteRec;
import Libs.Record.NewRec;
import Libs.Record.Record;
import Libs.Record.UpdateRec;
import Libs.Utils.Donnees;
import Libs.Utils.PassWord;
import Libs.Utils.utils;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;

/**
 * <p>Title: HardwareStore </p>
 * <p>Description: The HardwareStore application is a program that is used to display
 * hardware items and it allows these items to be created, diagUpdated, and/or
 * deleted.</p>
 * <p>Copyright: Copyright (c)</p>
 * <p>Company: TAW</p>
 *
 * @author unascribed
 * @version 2.0
 */
public class HardwareStore extends JFrame implements ActionListener {

    private static final int ZERO = 0;
    public PassWord diagPassWord;
    public UpdateRec diagUpdate;
    public NewRec diagNewRec;
    public DeleteRec diagDeleteRec;
    private Record data;
    public String[][] pData = new String[250][7];

    private MenuHandler menuHandler = new MenuHandler(this);
    private JTable table;
    private RandomAccessFile dataFile;
    /**
     * file from which data is read
     */
    public File aFile;
    private JButton btnCancel, btnRefresh;
    public boolean myDebug = false;
    /**
     * This flag toggles debug
     */
    HardwareStore hws;

    private Container container;
    private int numEntries = 0;

    public static void main(String[] args) {
        new HardwareStore().hws = new HardwareStore();
    }

    public HardwareStore() {
        super("Hardware Store: Lawn Mower ");

        data = new Record();
        aFile = new File("lawnmower.dat");
        container = getContentPane();

        setupMenu();
        InitRecord("lawnmower.dat", Donnees.lawnMower, 27);
        InitRecord("lawnTractor.dat", Donnees.lawnTractor, 11);
        InitRecord("handDrill.dat", Donnees.handDrill, 15);
        InitRecord("drillPress.dat", Donnees.drillPress, 10);
        InitRecord("circularSaw.dat", Donnees.circularSaw, 12);
        InitRecord("hammer.dat", Donnees.hammer, 12);
        InitRecord("tableSaw.dat", Donnees.tableSaw, 15);
        InitRecord("bandSaw.dat", Donnees.bandSaw, 10);
        InitRecord("sanders.dat", Donnees.sanders, 15);
        InitRecord("stapler.dat", Donnees.stapler, 15);
        setup();

        addWindowListener(new WindowHandler(this));
        setSize(700, 400);
        setVisible(true);
    }

    public void setupMenu() {
        // Create the menubar */
        JMenuBar menuBar = new JMenuBar();
        setJMenuBar(menuBar);

        // Create the File menu and add it to the menubar  */
        JMenu fileMenu = new JMenu("File");

        menuBar.add(fileMenu);
        // Add the Exit menuItems */
        // File Menu Items
        JMenuItem exitMenuItem = new JMenuItem("Exit");
        fileMenu.add(exitMenuItem);
        exitMenuItem.addActionListener(menuHandler);

        // Create the View menu and add it to the menubar  */
        JMenu viewMenu = new JMenu("View");

        // Add the Lawn Mower menuitems */
        // View Menu Items
        JMenuItem lawnMowMenuItem = new JMenuItem("Lawn Mowers");
        viewMenu.add(lawnMowMenuItem);
        lawnMowMenuItem.addActionListener(menuHandler);

        // Add the Lawn Mower menuitems */
        JMenuItem lawnMowTractMenuItem = new JMenuItem("Lawn Mowing Tractors");
        viewMenu.add(lawnMowTractMenuItem);
        lawnMowTractMenuItem.addActionListener(menuHandler);

        // Add the Hand Drills Tools menuitems */
        JMenuItem handDrillMenuItem = new JMenuItem("Hand Drills Tools");
        viewMenu.add(handDrillMenuItem);
        handDrillMenuItem.addActionListener(menuHandler);

        // Add the Drill Press Power Tools menuitems */
        JMenuItem drillPressPowMenuItem = new JMenuItem("Drill Press Power Tools");
        viewMenu.add(drillPressPowMenuItem);
        drillPressPowMenuItem.addActionListener(menuHandler);

        // Add the Circular Saws  menuitems */
        JMenuItem circSawMenuItem = new JMenuItem("Circular Saws");
        viewMenu.add(circSawMenuItem);
        circSawMenuItem.addActionListener(menuHandler);

        // Add the Hammer menuitems */
        JMenuItem hammerMenuItem = new JMenuItem("Hammers");
        viewMenu.add(hammerMenuItem);
        hammerMenuItem.addActionListener(menuHandler);

        // Add the Table Saws menuitems */
        JMenuItem tableSawMenuItem = new JMenuItem("Table Saws");
        viewMenu.add(tableSawMenuItem);
        tableSawMenuItem.addActionListener(menuHandler);

        // Add the Band Saws menuitems */
        JMenuItem bandSawMenuItem = new JMenuItem("Band Saws");
        viewMenu.add(bandSawMenuItem);
        bandSawMenuItem.addActionListener(menuHandler);

        // Add the Sanders menuitems */
        JMenuItem sanderMenuItem = new JMenuItem("Sanders");
        viewMenu.add(sanderMenuItem);
        sanderMenuItem.addActionListener(menuHandler);


        // Add the Stapler menuitems */
        JMenuItem staplerMenuItem = new JMenuItem("Staplers");
        viewMenu.add(staplerMenuItem);
        staplerMenuItem.addActionListener(menuHandler);

        // Add the Wet-Dry Vacs menuitems */
        JMenuItem wetDryMenuItem = new JMenuItem("Wet-Dry Vacs");
        viewMenu.add(wetDryMenuItem);
        wetDryMenuItem.addActionListener(menuHandler);

        // Add the Storage, Chests & Cabinets menuitems */
        JMenuItem storChestCabMenuItem = new JMenuItem("Storage, Chests & Cabinets");
        viewMenu.add(storChestCabMenuItem);
        storChestCabMenuItem.addActionListener(menuHandler);

        menuBar.add(viewMenu);
        // Create the Options menu and add it to the menubar  */
        JMenu optionsMenu = new JMenu("Options");

        // Add the List All menuitems */
        JMenuItem listAllMenuItem = new JMenuItem("List All");
        optionsMenu.add(listAllMenuItem);
        listAllMenuItem.addActionListener(menuHandler);
        optionsMenu.addSeparator();

        // Add the Add menuitems */
        JMenuItem addMenuItem = new JMenuItem("Add");
        optionsMenu.add(addMenuItem);
        addMenuItem.addActionListener(menuHandler);

        // Add the Update menuitems */
        JMenuItem updateMenuItem = new JMenuItem("Update");
        optionsMenu.add(updateMenuItem);
        updateMenuItem.addActionListener(menuHandler);
        optionsMenu.addSeparator();

        // Add the Delete menuitems */
        // Options Menu Items
        JMenuItem deleteMenuItem = new JMenuItem("Delete");
        optionsMenu.add(deleteMenuItem);
        deleteMenuItem.addActionListener(menuHandler);

        menuBar.add(optionsMenu);

        // Create the Tools menu and add it to the menubar  */
        JMenu toolsMenu = new JMenu("Tools");
        menuBar.add(toolsMenu);
        // Add the Tools menuitems */
        // Tools Menu Items
        JMenuItem debugOnMenuItem = new JMenuItem("Debug On");
        JMenuItem debugOffMenuItem = new JMenuItem("Debug Off");
        toolsMenu.add(debugOnMenuItem);
        toolsMenu.add(debugOffMenuItem);
        debugOnMenuItem.addActionListener(menuHandler);
        debugOffMenuItem.addActionListener(menuHandler);

        // Create the Help menu and add it to the menubar  */
        JMenu helpMenu = new JMenu("Help");

        // Add the Help HW Store menuitems */
        // Help Menu Items
        JMenuItem helpMenuItem = new JMenuItem("Help on HW Store");
        helpMenu.add(helpMenuItem);
        helpMenuItem.addActionListener(menuHandler);

        menuBar.add(helpMenu);

        // Create the About menu and add it to the menubar  */
        JMenu aboutMenu = new JMenu("About");

        // Add the About Store menuitems */
        // About Menu Items
        JMenuItem aboutMenuItem = new JMenuItem("About HW Store");
        aboutMenu.add(aboutMenuItem);
        aboutMenuItem.addActionListener(menuHandler);

        menuBar.add(aboutMenu);
    }

    /**
     * *******************************************************
     * Method: setup() is used to
     * 1- Open the lawnmower.dat file
     * 2- Call the toArray() method to popualte the JTable with
     * the contents of the lawnmower.dat file.
     * <p>
     * Called by the HardwareStore() constructor
     ********************************************************/
    public void setup() {
        data = new Record();

        try {
            /* Divide  the length of the file by the record size to
             *  determine the number of records in the file
             */

            dataFile = new RandomAccessFile("lawnmower.dat", "rw");
            aFile = new File("lawnmower.dat");
            numEntries = toArray(dataFile, pData);
            dataFile.close();
        } catch (IOException ex) {
            ex.printStackTrace();
        }

        /* ****************************************************************
         * pData contains the data to be loaded into the JTable.
         * columnNames contains the column names for the table.
         * 1- Create a new JTable.
         * 2- Add a mouse listener to the table.
         * 3- Make the table cells editable.
         * 4- Add the table to the Frame's center using the Border Layout
         *    Manager.
         * 5- Add a scrollpane to the table.
         *****************************************************************/
        table = new JTable(pData, Donnees.columnNames);
        table.addMouseListener(new MouseClickedHandler(dataFile, table, pData));
        table.setEnabled(true);

        container.add(table, BorderLayout.CENTER);
        container.add(new JScrollPane(table));

        // Make the Frame visible
        btnCancel = new JButton("Cancel");
        btnRefresh = new JButton("Refresh");
        JPanel buttonPanel = new JPanel();
        buttonPanel.add(btnCancel);
        container.add(buttonPanel, BorderLayout.SOUTH);

        btnRefresh.addActionListener(this);
        btnCancel.addActionListener(this);

        // Create dialog boxes
        diagUpdate = new UpdateRec(hws, dataFile, pData, -1);
        diagDeleteRec = new DeleteRec(hws, dataFile, table, pData);
        /* Allocate diagPassWord last; otherwise  the diagUpdate,
         *  diagNewRec and diagDeleteRec references will be null
         *  when the PassWrod class attempts to use them.*/
        diagPassWord = new PassWord(this);
    }

    /**
     * ***************************************************************
     * Method; InitRecord() is used to create and initialize the
     * tables used by the Hardware Store application. The parameters
     * passed are:
     * 1- String fileDat: is the name of the file to be created and
     * initialized.
     * 2- String FileRecord[][]: is the two dimensional array that contains
     * the initial data.
     * 3- int loopCtl: is the number of entries in the array.
     * <p>
     * Called by the HardwareStore() constructor
     ******************************************************************/
    public void InitRecord(String fileDat, String[][] FileRecord, int loopCtl) {

        aFile = new File(fileDat);
        utils.debugMessage("initRecord(): 1a - the value of fileData is " + aFile);
        try {
            /* Open the fileDat file in RW mode.
             *  If the file does not exist, create it
             *  and initialize it to 250 empty records.
             */

            utils.debugMessage("initTire(): 1ab - checking to see if " + aFile + " exist.");
            if (!aFile.exists()) {

                utils.debugMessage("initTire(): 1b - " + aFile + " does not exist.");

                dataFile = new RandomAccessFile(aFile, "rw");
                data = new Record();

                for (int ii = 0; ii < loopCtl; ii++) {
                    data.setRecID(Integer.parseInt(FileRecord[ii][0]));
                    utils.debugMessage("initTire(): 1c - The value of record ID is " + data.getRecID());
                    data.setToolType(FileRecord[ii][1]);
                    utils.debugMessage("initTire(): 1cb - The length of ToolType is " + data.getToolType().length());
                    data.setBrandName(FileRecord[ii][2]);
                    data.setToolDesc(FileRecord[ii][3]);
                    utils.debugMessage("initTire(): 1cc - The length of ToolDesc is " + data.getToolDesc().length());
                    data.setPartNumber(FileRecord[ii][4]);
                    data.setQuantity(Integer.parseInt(FileRecord[ii][5]));
                    data.setCost(FileRecord[ii][6]);

                    utils.debugMessage("initTire(): 1d - Calling Record method write() during initialization. " + ii);
                    dataFile.seek(ii * Record.SIZE);
                    data.write(dataFile);

                }
            } else {
                utils.debugMessage("initTire(): 1e - " + fileDat + " exists.");
                dataFile = new RandomAccessFile(aFile, "rw");
            }

            dataFile.close();
        } catch (IOException e) {
            System.err.println("InitRecord() " + e.toString() +
                    " " + aFile);
            System.exit(1);
        }
    }

    /**
     * ************************************************************
     * Method: display() is used to display the contents of the
     * specified table in the passed parameter. This method
     * uses the passed parameter to determine
     * 1- Which table to display
     * 2- Whether the table exists
     * 3- If it exists, the table is opened and its
     * contents are displayed in a JTable.
     * <p>
     * Called from the actionPerformed() method of the MenuHandler class
     *********************************************************************/
    public void display(String str) {

        String df = str.trim().toLowerCase()+".dat";

        switch (str) {

            case "Lawn Mowers":
                df = "lawnmower.dat";
                break;
            case "Lawn Tractor Mowers":
                df = "lawnTractor.dat";
                break;
            case "Hand Drill Tools":
                df = "handDrill.dat";
                break;
            case "Drill Press Power Tools":
                df = "drillPress.dat";
                break;
            case "Circular Saws":
                df = "circularSaw.dat";
                break;
            case "Hammers":
                df = "hammer.dat";
                break;
            case "Table Saws":
                df = "tableSaw.dat";
                break;
            case "Band Saws":
                df = "bandSaw.dat";
                break;
            case "Sanders":
                df = "sanders.dat";
                break;
            case "Staplers":
                df = "stapler.dat";
                break;
        }

        try {
            if (df == null)
                throw new Exception("Seem to not be in our list :T");

            String title = "Hardware Store: "+str;
            aFile = new File("circularSaw.dat");
            /* Open the .dat file in RW mode.
             *  If the file does not exist, create it
             *  and initialize it to 250 empty records.
             */

            utils.debugMessage("display(): 1a - checking to see if " + df + " exists.");
            if (!aFile.exists()) {
                utils.debugMessage("display(): 1b - " + df + " does not exist.");
            } else {
                dataFile = new RandomAccessFile(df, "rw");
                this.setTitle(title);
                redisplay(dataFile, pData);
            }

            dataFile.close();
        } catch (Exception e) {
            System.err.println(e.toString());
            System.err.println("Failed in opening " + df);
            System.exit(1);
        }
    }

    /**
     * *******************************************************
     * Method: redisplay() is used to redisplay/repopualte the
     * JTable.
     * <p>
     * Called from the
     * 1- display() method
     * 2- actionPerformed() method of the UpdateRec class
     * 3- actionPerformed() method of the DeleteRec class
     ********************************************************/
    public void redisplay(RandomAccessFile dataFile, String[][] a) {
        for (int ii = 0; ii < numEntries + 5; ii++) {
            a[ii][0] = "";
            a[ii][1] = "";
            a[ii][2] = "";
            a[ii][3] = "";
            a[ii][4] = "";
            a[ii][5] = "";
            a[ii][6] = "";
        }

        int entries = toArray(dataFile, a);
        utils.debugMessage("redisplay(): 1  - The number of entries is " + entries);
        setEntries(entries);
        container.remove(table);
        table = new JTable(a, Donnees.columnNames);
        table.setEnabled(true);
        container.add(table, BorderLayout.CENTER);
        container.add(new JScrollPane(table));
        container.validate();
    }

    public void actionPerformed(ActionEvent e) {

        if (e.getSource() == btnRefresh) {
            utils.debugMessage("\nThe Refresh button was pressed. ");
            Container container = getContentPane();

            table = new JTable(pData, Donnees.columnNames);
            container.validate();
        } else if (e.getSource() == btnCancel) {
            cleanup();
        }
    }

    public void cleanup() {
        try {
            dataFile.close();
        } catch (IOException e) {
            System.exit(1);
        }

        setVisible(false);
        System.exit(0);
    }

    public void displayDeleteDialog() {
        utils.debugMessage("The Delete Record Dialog was made visible.\n");
        diagDeleteRec.setVisible(true);
    }

    public void displayUpdateDialog() {
        utils.debugMessage("The Update Record Dialog was made visible.\n");
        JOptionPane.showMessageDialog(null,
                "Enter the record ID to be diagUpdated and press enter.",
                "Update Record", JOptionPane.INFORMATION_MESSAGE);
        diagUpdate = new UpdateRec(hws, dataFile, pData, -1);
        diagUpdate.setVisible(true);
    }

    public void displayAddDialog() {
        utils.debugMessage("The New/Add Record Dialog was made visible.\n");
        diagNewRec = new NewRec(hws, dataFile, table, pData);
        diagNewRec.setVisible(true);
    }

    public void setEntries(int ent) {
        numEntries = ent;
    }

    public int getEntries() {
        return numEntries;
    }

    public int toArray(RandomAccessFile dataFile, String[][] a) {
        Record nodeRef = new Record();
        int ii = 0, row = 0, fileSize = 0;

        try {
            fileSize = (int) dataFile.length() / Record.SIZE;
            utils.debugMessage("toArray(): 1 - The size of the file is " + fileSize);
            /* If the file is empty, do nothing.  */
            if (fileSize > ZERO) {

                nodeRef.setFileLen(dataFile.length());
                while (ii < fileSize) {
                    utils.debugMessage("toArray(): 2 - NodeRef.getRecID is "
                            + nodeRef.getRecID());

                    dataFile.seek(0);
                    dataFile.seek(ii * Record.SIZE);
                    nodeRef.setFilePos(ii * Record.SIZE);
                    utils.debugMessage("toArray(): 3 - input data file - Read record " + ii);
                    nodeRef.ReadRec(dataFile);

                    utils.debugMessage("toArray(): 4 - the value of a[ ii ] [ 0 ] is " + a[0][0]);

                    if (nodeRef.getRecID() != -1) {
                        a[row][0] = String.valueOf(nodeRef.getRecID());
                        a[row][1] = nodeRef.getToolType().trim();
                        a[row][2] = nodeRef.getBrandName().trim();
                        a[row][3] = nodeRef.getToolDesc().trim();
                        a[row][4] = nodeRef.getPartNumber().trim();
                        a[row][5] = String.valueOf(nodeRef.getQuantity());
                        a[row][6] = nodeRef.getCost().trim();

                        utils.debugMessage("toArray(): 5 - 0- " + a[row][0],
                                " 1- " + a[row][1],
                                " 2- " + a[row][2],
                                " 3- " + a[row][3],
                                " 4- " + a[row][4],
                                " 5- " + a[row][5],
                                " 6- " + a[row][6]);

                        row++;

                    } else {
                        utils.debugMessage("toArray(): 5a the record ID is " + ii);
                    }

                    ii++;
                }
            }
        } catch (IOException ex) {
            utils.debugMessage("toArray(): 6 - input data file failure. Index is " + ii
                    + "\nFilesize is " + fileSize);
        }

        return ii;
    }
}
