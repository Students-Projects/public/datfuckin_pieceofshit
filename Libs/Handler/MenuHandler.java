package Libs.Handler;

import Libs.Record.DeleteRec;
import Libs.Record.UpdateRec;
import Libs.Store.HardwareStore;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;

import javax.swing.JMenuItem;
import javax.swing.JTable;

public class MenuHandler implements ActionListener {
    // File Menu Items
    private JMenuItem exitMenuItem;

    // View Menu Items
    private JMenuItem lawnMowMenuItem, lawnMowTractMenuItem, handDrillMenuItem, drillPressPowMenuItem, hammerMenuItem,
            circSawMenuItem, tableSawMenuItem, bandSawMenuItem, sanderMenuItem, staplerMenuItem, wetDryMenuItem, storChestCabMenuItem;

    // Options Menu Items
    private JMenuItem deleteMenuItem, addMenuItem, updateMenuItem, listAllMenuItem;

    // Tools Menu Items
    private JMenuItem debugOnMenuItem, debugOffMenuItem;

    // Help Menu Items
    private JMenuItem helpMenuItem;

    // About Menu Items
    private JMenuItem aboutMenuItem;
    private JTable table;
    private RandomAccessFile dataFile;
    /**
     * file from which data is read
     */
    public File aFile;
    protected boolean validPW = false;
    HardwareStore hws;

    public MenuHandler(HardwareStore hws) {
        this.hws = hws;
    }

    public void actionPerformed(ActionEvent e) {

        if (e.getSource() == exitMenuItem) {
            /**The Exit menu Item was selected. */
            hws.cleanup();
        } else if (e.getSource() == lawnMowMenuItem) {
            hws.sysPrint("The Lawn Mower menu Item was selected.\n");

            hws.display("Lawn Mowers");
        } else if (e.getSource() == lawnMowTractMenuItem) {
            hws.sysPrint("The Lawn Mower Tractor menu Item was selected.\n");

            hws.display("Lawn Tractor Mowers");
        } else if (e.getSource() == handDrillMenuItem) {
            hws.sysPrint("The Hand Drill Tools menu Item was selected.\n");

            hws.display("Hand Drill Tools");
        } else if (e.getSource() == drillPressPowMenuItem) {
            hws.sysPrint("The Drill Press Power Tools menu Item was selected.\n");

            hws.display("Drill Press Power Tools");
        } else if (e.getSource() == circSawMenuItem) {
            hws.sysPrint("The Circular Saws Tools menu Item was selected.\n");

            hws.display("Circular Saws");
        } else if (e.getSource() == hammerMenuItem) {
            hws.sysPrint("The Hammer menu Item was selected.\n");

            hws.display("Hammers");
        } else if (e.getSource() == tableSawMenuItem) {
            hws.sysPrint("The Table Saws menu Item was selected.\n");

            hws.display("Table Saws");
        } else if (e.getSource() == bandSawMenuItem) {
            hws.sysPrint("The Band Saws menu Item was selected.\n");

            hws.display("Band Saws");
        } else if (e.getSource() == sanderMenuItem) {
            hws.sysPrint("The Sanders menu Item was selected.\n");

            hws.display("Sanders");
        } else if (e.getSource() == staplerMenuItem) {
            hws.sysPrint("The Staplers menu Item was selected.\n");

            hws.display("Staplers");
        } else if (e.getSource() == wetDryMenuItem) {
            hws.sysPrint("The Wet-Dry Vacs menu Item was selected.\n");
            // ListRecs BPTRecs = new ListRecs( hws , "WDV", "Wet-Dry Vacs" );
        } else if (e.getSource() == storChestCabMenuItem) {
            hws.sysPrint("The Storage, Chests & Cabinets menu Item was selected.\n");
            //ListRecs BPTRecs = new ListRecs( hws , "SCC", "Storage, Chests & Cabinets" );
        } else if (e.getSource() == deleteMenuItem) {
            hws.sysPrint("The Delete Record Dialog was made visible.\n");
            //DeleteRec( HardwareStore hw_store,  RandomAccessFile f,
            // JTable tab, String p_Data[] []  )
            hws.diagDeleteRec = new DeleteRec(hws, dataFile, table, hws.pData);
            hws.diagDeleteRec.setVisible(true);
        } else if (e.getSource() == addMenuItem) {
            hws.sysPrint("The Add menu Item was selected.\n");
            hws.diagPassWord.displayDialog("add");
        } else if (e.getSource() == updateMenuItem) {
            hws.sysPrint("The Update menu Item was selected.\n");
            hws.diagUpdate = new UpdateRec(hws, dataFile, hws.pData, -1);
            hws.diagUpdate.setVisible(true);
        } else if (e.getSource() == listAllMenuItem) {
            hws.sysPrint("The List All menu Item was selected.\n");
            //listRecs.setVisible( true );
        } else if (e.getSource() == debugOnMenuItem) {
            hws.myDebug = true;
            hws.sysPrint("Debugging for this execution is turned on.\n");
        } else if (e.getSource() == debugOffMenuItem) {
            hws.sysPrint("Debugging for this execution is turned off.\n");
            hws.myDebug = false;
        } else if (e.getSource() == helpMenuItem) {
            hws.sysPrint("The Help menu Item was selected.\n");
            File hd = new File("HW_Tutorial.html");
            //File net = new File("Netscp.exe");
            //System.out.println( "the path for help_doc is " + hd.getAbsolutePath() );
            //System.out.println( "the path for netscape is " + net.getAbsolutePath() );

            Runtime rt = Runtime.getRuntime();
            //String[] callAndArgs = { "d:\\Program Files\\netscape\\netscape\\Netscp.exe" ,
            String[] callAndArgs = {"c:\\Program Files\\Internet Explorer\\IEXPLORE.exe",
                    "" + hd.getAbsolutePath()};

            try {

                Process child = rt.exec(callAndArgs);
                child.waitFor();
                hws.sysPrint("Process exit code is: " +
                        child.exitValue());
            } catch (IOException e2) {
                hws.sysPrint(
                        "IOException starting process!");
            } catch (InterruptedException e3) {
                System.err.println(
                        "Interrupted waiting for process!");
            }
        } else if (e.getSource() == aboutMenuItem) {
            hws.sysPrint("The About menu Item was selected.\n");
            Runtime rt = Runtime.getRuntime();
            String[] callAndArgs = {"c:\\Program Files\\Internet Explorer\\IEXPLORE.exe",
                    "http://www.sumtotalz.com/TotalAppsWorks/ProgrammingResource.html"};
            try {
                Process child = rt.exec(callAndArgs);
                child.waitFor();
                hws.sysPrint("Process exit code is: " +
                        child.exitValue());
            } catch (IOException e2) {
                System.err.println(
                        "IOException starting process!");
            } catch (InterruptedException e3) {
                System.err.println(
                        "Interrupted waiting for process!");
            }
        }
    }
}