package Libs.Handler;

import Libs.Record.UpdateRec;
import Libs.Store.HardwareStore;

import javax.swing.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.RandomAccessFile;

/** ********************************************************
    *  Class:
    ********************************************************/
   public class MouseClickedHandler extends MouseAdapter {
      JTable table;
      String pData[] [], columnNames[];
      RandomAccessFile f;
      HardwareStore hws;
      
      public MouseClickedHandler(HardwareStore hws) {
    	  this.hws = hws;
      }

   /** ********************************************************
    *  Method:
    ********************************************************/
   public MouseClickedHandler(RandomAccessFile fPassed, JTable tablePassed, String p_Data[][]) {
          table = tablePassed;
          pData = p_Data;
          f     = fPassed;
      }
   /** ********************************************************
    *  Method: mouseClicked()
    ********************************************************/
      public void mouseClicked(MouseEvent e)    {
         if (e.getSource() == table) {
             int ii = table.getSelectedRow() ;
             JOptionPane.showMessageDialog(null,
                    "Enter the record ID to be diagUpdated and press enter.",
                    "Update Record", JOptionPane.INFORMATION_MESSAGE);
             UpdateRec diagUpdate = new UpdateRec(hws, f , pData , ii);
             if (ii < 250) {
                diagUpdate.setVisible( true );
                table.repaint();
             }
         }
      }
   }
