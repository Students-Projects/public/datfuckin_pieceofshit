
# Record

## Read Record
_Method: `ReadRec()` Reads a record from the specified RandomAccessFile._
1. Read the first integer
2. Read the second integer
3. Read characters one at a time until we reach a string of
`';;;'`. This indicates that we have reached the end of the
character string for this particular record.
4. Load the resulting string into a StringTokenizer object.
5. We are looking for 7 tokens, so if the token count is
greater than 4, we will tokenize the string.
6. The tokens are loaded into a string array and then into the
class variables.

## Fill
_The `fill()` method is used to fill in the passed string with blanks._

## Write
### Default
_`write()` Writes a record to the specified RandomAccessFile._
1. First it writes a int (recid) to the output file
2. Next it writes the quantity as an int.
3. Then it writes the remaing record as a string.

### Integer
_Method: `writeInteger()` is used to wite an integer to the randomaccess file._

